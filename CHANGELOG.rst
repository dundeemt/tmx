Changelog
=========

0.0.3 | 2015-12-13
------------------

* added "name" to session level attributes. Allow you to name the first window
  of the session without creating a windows level definition.  See README.rst
* expanding test coverage, refactor supporting bld_* functions to be more
  robust in dealing with missing attributes.

0.0.2 | 2015-12-11
------------------

* Added "size" attribute to those available for a pane. See README.rst
* Work on expanding test coverage continues.


0.0.1 | 2015-12-08
------------------
* Initial public launch of tmx

