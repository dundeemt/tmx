'''test handle_meta_configs function'''
import json
import os
import sys
from tempfile import NamedTemporaryFile
sys.path.insert(0, '..')
import tmx      # pylint:disable=F0401

import pytest


FMETA = 'meta.json'
MFILES = [os.path.expanduser(x) for x in json.loads(open(FMETA).read())]

# pylint:disable=r0201,w0232,c0111,c1001,r0903
class TestHandleMetaConfigs:

    def test_expansion(self):
        '''test expansion of meta file'''
        assert tmx.handle_meta_configs([FMETA, ]) == MFILES


    def test_meta_first(self):
        '''test meta file appearing first in list'''
        result = MFILES + ['minimal.json']
        assert tmx.handle_meta_configs([FMETA, 'minimal.json']) == result


    def test_meta_last(self):
        '''test meta file appearing last in list'''
        result = ['minimal.json'] + MFILES
        assert tmx.handle_meta_configs(['minimal.json', FMETA]) == result


    def test_meta_meta(self):
        '''test meta file containing a link to another meta file'''
        assert tmx.handle_meta_configs(['meta-meta.json']) == MFILES


class TestHandleDebugFlag:
    '''test the --debug flag handling'''

    def test_first(self):
        '''test that it works in first postion'''
        args = ['--debug', 'one', 'two']
        rslt = tmx.handle_debug_flag(args)
        assert tmx.DEBUG is True
        assert rslt == args[1:]


    def test_middle(self):
        '''test that it works in the body of the args'''
        args = ['one', '--debug', 'two']
        rslt = tmx.handle_debug_flag(args)
        assert tmx.DEBUG is True
        assert rslt == [args[0], args[2]]


    def test_last(self):
        '''test that it works with debug at the end'''
        args = ['one', 'two', '--debug']
        rslt = tmx.handle_debug_flag(args)
        assert tmx.DEBUG is True
        assert rslt == args[:-1]


    def test_none(self):
        '''test that by default DEBUG stays off'''
        args = ['one', 'two', 'three']
        rslt = tmx.handle_debug_flag(args)
        assert tmx.DEBUG is False
        assert rslt == args


class TestGetColors:
    '''test the get_colors function'''
    def test_empty(self):
        '''test that it can handle an empty config'''
        assert tmx.get_colors({}) == '-2'

    def test_none(self):
        '''setting colors:None -> empty string'''
        assert tmx.get_colors({'colors': None}) == ''

    def test_88(self):
        '''set colors:88 results in -8'''
        assert tmx.get_colors({'colors': 88}) == '-8'

    def test_funky(self):
        '''anything else results in a -2'''
        assert tmx.get_colors({'colors': 'foo'}) == '-2'
        assert tmx.get_colors({'colors': 256}) == '-2'


class TestBldNewSession:
    '''test the bld_new_session function'''
    # tmux [-2] new-session -d [-n name] -s session_name
    @pytest.mark.parametrize("cfg, rslt",
                            [({'session-name': 'foo'},
                              ['tmux', '-2', 'new-session',
                               '-d', '-s', 'foo']),
                             ({'session-name': 'foo',
                               'colors': 88},
                              ['tmux', '-8', 'new-session',
                               '-d', '-s', 'foo']),
                             ({'session-name': 'foo',
                               'colors': None},
                              ['tmux', 'new-session',
                               '-d', '-s', 'foo']),
                             ({'session-name': 'foo',
                               'name': 'bar'},
                              ['tmux', '-2', 'new-session', '-d', '-n', 'bar',
                               '-s', 'foo']),
                            ])      # pylint:disable=e1101
    def test_options(self, cfg, rslt):
        assert tmx.bld_new_session(['tmux'], 'new-session', cfg) == rslt


class TestBldNewWindow:
    '''tmux new-window -c -|cwd [-n name]'''
    @pytest.mark.parametrize("cfg, rslt", [
        ({}, ['tmux', 'new-window', '-c', '-']),
        ({'cwd': 'foo'}, ['tmux', 'new-window', '-c', 'foo']),
        ({'name': 'bar'}, ['tmux', 'new-window', '-c', '-', '-n', 'bar']),
        ({'cwd': 'foo', 'name': 'bar'}, ['tmux', 'new-window', '-c', 'foo',
                                         '-n', 'bar'])
    ])
    def test_bld_new_window(self, cfg, rslt):
        assert tmx.bld_new_window(['tmux'], 'new-window', cfg) == rslt


class TestBldRenameWindow:
    '''if name, then send command else empty cmd'''
    @pytest.mark.parametrize("cfg, rslt", [
        ({'name': 'foo'}, ['tmux', 'rename-window', 'foo']),
        ({}, [])
    ])
    def test_options(self, cfg, rslt):
        assert tmx.bld_rename_window(['tmux'], 'rename-window', cfg) == rslt


class TestBldSendKeys:
    '''tmux send-keys keys C-m'''
    @pytest.mark.parametrize("cfg, rslt", [
        ({}, []),
        ({'keys': ''}, []),
        ({'keys': None}, []),
        ({'keys': 'cd foo'}, ['tmux', 'send-keys', 'cd foo', 'C-m'])
    ])
    def test_options(self, cfg, rslt):
        assert tmx.bld_send_keys(['tmux'], 'send-keys', cfg) == rslt


class TestBldSplitWindow:
    '''tmux split-window -v|-h -c -|cwd [-l|-p size]'''
    @pytest.mark.parametrize("cfg, rslt", [
        ({}, ['tmux', 'split-window', '-v', '-c', '-']),
        ({'split': '|'}, ['tmux', 'split-window', '-h', '-c', '-']),
        ({'split': 'h'}, ['tmux', 'split-window', '-h', '-c', '-']),
        ({'split': 'HoRiZoNtAl'}, ['tmux', 'split-window', '-h', '-c', '-']),
        ({'split': '-'}, ['tmux', 'split-window', '-v', '-c', '-']),
        ({'split': '-v'}, ['tmux', 'split-window', '-v', '-c', '-']),
        ({'cwd': '/temp'}, ['tmux', 'split-window', '-v', '-c', '/temp']),
        ({'cwd': '~'}, ['tmux', 'split-window', '-v', '-c',
                        os.path.expanduser('~')]),
        ({'size': 22}, ['tmux', 'split-window', '-v', '-c', '-', '-l', '22']),
        ({'size': '22'}, ['tmux', 'split-window', '-v', '-c', '-', '-l', '22']),
        ({'size': '22%'}, ['tmux', 'split-window', '-v', '-c', '-', '-p',
                           '22']),
    ])
    def test_options(self, cfg, rslt):
        assert tmx.bld_split_window(['tmux'], 'split-window', cfg) == rslt


class TestBldSelectWindow:
    '''tmux select-window [-lnpT]|-t target'''
    @pytest.mark.parametrize("cfg, rslt", [
        ({}, ['tmux', 'select-window', '-n']),
        ({'target': '-n'}, ['tmux', 'select-window', '-n']),
        ({'target': 0}, ['tmux', 'select-window', '-t', '0']),
        ({'target': 'fred'}, ['tmux', 'select-window', '-t', 'fred']),
    ])
    def test_option(self, cfg, rslt):
        assert tmx.bld_select_window(['tmux'], 'select-window', cfg) == rslt


class TestBldSelectLayout:
    '''tmux select-layout layout'''
    def setup(self):
        # mock up some named layouts
        tmx.NAMED_LAYOUTS = {'foo': 'foo123', 'bar': 'bar123'}

    @pytest.mark.parametrize("cfg, rslt", [
        ({}, []),
        ({'layout': 'even-horizontal'}, ['tmux', 'select-layout',
                                         'even-horizontal']),
        ({'layout': 'foo'}, ['tmux', 'select-layout', 'foo123'])
    ])
    def test_option(self, cfg, rslt):
        assert tmx.bld_select_layout(['tmux'], 'select-layout', cfg) == rslt


class TestBldSelectPane:
    '''tmux select-pane -t pane_id'''
    @pytest.mark.parametrize("cfg, rslt", [
        ({'pane_id': '0'}, ['tmux', 'select-pane', '-t', '0']),
        ({'pane_id': 0}, ['tmux', 'select-pane', '-t', '0']),
    ])
    def test_option(self, cfg, rslt):
        assert tmx.bld_select_pane(['tmux'], 'select-pane', cfg) == rslt


class TestBldAttachSession:
    '''tmux [-2|-8] attach-session -t session-name'''
    @pytest.mark.parametrize("cfg, rslt", [
        ({'session-name': 'foo'}, ['tmux', '-2', 'attach-session', '-t',
                                   'foo']),
        ({'session-name': 'foo', 'colors': None}, ['tmux', 'attach-session',
                                                   '-t', 'foo'])
    ])
    def test_options(self, cfg, rslt):
        assert tmx.bld_attach_session(['tmux'], 'attach-session', cfg) == rslt


class TestBuildCommand:
    '''test that build command responds correctly on bad cmd_name'''
    def test_bad_cmd_name(self):
        with pytest.raises(SystemExit):
            tmx.build_command('white_rabbit', {})


class TestDoSession:
    '''test that session-name is required'''
    def test_missing_session_name(self):
        with pytest.raises(SystemExit):
            tmx.do_session({})


class TestPopulateNamedLayouts:
    '''check that user named layouts are properly read from tmuxconf'''
    def setup(self):
        tmx.NAMED_LAYOUTS = {}      # make sure named layouts are empty

    def test_conf_not_found(self):
        with NamedTemporaryFile() as h_temp:
            bad_filename = h_temp.name
        # file is closed and deleted now
        assert os.path.exists(bad_filename) is False
        tmx.populate_named_layouts(tmuxconf=bad_filename)
        assert tmx.NAMED_LAYOUTS == {}

    def test_no_named_layouts(self):
        '''exists but no named layouts'''
        h_temp = NamedTemporaryFile(delete=False)
        h_temp.write('a line\n# a comment\n')
        h_temp.close()
        tmx.populate_named_layouts(tmuxconf=h_temp.name)
        os.unlink(h_temp.name)
        assert tmx.NAMED_LAYOUTS == {}

    def test_badly_formatted_not_named(self):
        '''named layouts are not named'''
        h_temp = NamedTemporaryFile(delete=False)
        h_temp.write('a line\n# a comment\n')
        h_temp.write('# named-layout:\n')
        h_temp.write('# named-layout:foo:bar')
        h_temp.close()
        tmx.populate_named_layouts(tmuxconf=h_temp.name)
        os.unlink(h_temp.name)
        assert tmx.NAMED_LAYOUTS == {'foo': 'bar'}

    def test_multiple_named_layouts(self):
        '''multiple named layouts'''
        h_temp = NamedTemporaryFile(delete=False)
        h_temp.write('a line\n# a comment\n')
        h_temp.write('# named-layout:foo:bar\n')
        h_temp.write('# named-layout: bar : baz \n')
        h_temp.close()
        tmx.populate_named_layouts(tmuxconf=h_temp.name)
        os.unlink(h_temp.name)
        assert tmx.NAMED_LAYOUTS == {'foo': 'bar', 'bar': 'baz'}

