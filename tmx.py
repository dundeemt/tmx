#!/usr/bin/env python
"""tmux config"""
from __future__ import print_function

import json
import os
import subprocess
import sys

__version__ = "0.0.4"
__author__ = "Jeff Hinrichs"
DEBUG = False
NAMED_LAYOUTS = {}
TMUX_VERSION = subprocess.check_output(['tmux', '-V']).split()[1]

def handle_debug_flag(args):
    """look for the --debug flag and take action"""
    global DEBUG        # pylint:disable=w0603
    DEBUG = False       # start assuming it is off
    debug = '--debug'
    nargs = args[:]
    if debug in nargs:
        DEBUG = True
        del nargs[nargs.index(debug)]
    return nargs


def get_colors(cfg):
    """return the colors setting, default to 256 if not specified,
    or specified incorrectly."""
    colors = cfg.get('colors', 256)
    if colors == 88:
        colors = '-8'
    elif colors is None:
        colors = ''
    else:
        colors = '-2'
    return colors


def get_expanded_cwd(cfg):
    '''if cwd exists in cfg, expand and return, else False'''
    # somewhere between tmux 1.8 and 2.1, tmux started having problems
    # with expansions -- a double escape? so we do the expanding and give
    # tmux ane explicit path
    cwd = cfg.get('cwd', False)
    if cwd:
        cwd = os.path.expanduser(cwd)

    return cwd


def bld_new_session(cmd, cmd_name, cfg):
    """return a list containing values for a new-session"""
    # tmux [-2] new-session -d [-n name] -s session_name
    colors = get_colors(cfg)
    if colors:
        cmd.append(colors)
    cmd += [cmd_name, '-d']
    cwd = get_expanded_cwd(cfg)
    if cwd:
        if TMUX_VERSION >= '2.2':
            cmd +=['-c', cwd]
        else:
            os.chdir(cwd)
    name = cfg.get('name', '')
    if name:
        cmd += ['-n', name]
    cmd += ['-s', cfg['session-name']]
    return cmd


def tver(version, new_option, old_option):
    '''return new_option if TMUX_VERSION > version'''
    # tmux does some questionable things so sometimes an option
    # works just fine and the next release it's gone and replaced
    if TMUX_VERSION > version:
        return new_option
    else:
        return old_option


def bld_new_window(cmd, cmd_name, cfg):     # pylint:disable=w0613
    """return list containing values for new-window"""
    # tmux new-window -c -|cwd [-n name]
    cwd = get_expanded_cwd(cfg)
    if not cwd:
        # deal with tmux's punting on pathing on >1.8
        cwd = tver('1.8', "#{client_cwd}", '-')

    cmd += [cmd_name, '-c', cwd]
    name = cfg.get('name', '')
    if name:
        cmd += ['-n', name]
    return cmd


def bld_rename_window(cmd, cmd_name, cfg):
    """return list with values for rename-window"""
    # tmux rename-window name
    name = cfg.get('name', '')
    if name:
        cmd += [cmd_name, cfg['name']]
    else:
        cmd = []    # noop if not specified
    return cmd


def bld_send_keys(cmd, cmd_name, cfg):
    """return list with values for send-keys"""
    # tmux send-keys "keys to send" C-m
    keys = cfg.get('keys', '')
    if keys:
        cmd += [cmd_name, keys, 'C-m']
    else:
        cmd = []
    return cmd


def bld_split_window(cmd, cmd_name, cfg):
    """return list w/ values for split-window"""
    # tmux split-window -v|-h -c -|cwd [-l|-p size]
    if cfg.get('split', '-').lower() in ('|', 'h', 'horizontal'):
        split = '-h'
    else:
        split = '-v'
    cwd = get_expanded_cwd(cfg)
    if not cwd:
        # deal with tmux's punting on pathing on >1.8
        cwd = tver('1.8', "#{pane_current_path}", '-')
    cmd += [cmd_name, split, '-c', cwd]
    size = str(cfg.get('size', '')).strip()  # could be numeric, so cast to str
    if size:
        lop = '-l'
        if size.endswith('%'):
            lop = '-p'
            size = size[:-1]        # everything but the ending %
        cmd += [lop, size]
    return cmd


def bld_select_window(cmd, cmd_name, cfg):
    """return list w/ value for select-next-window, defaults to -n (next)"""
    # tmux select-window [-lnpT]|-t target
    target = str(cfg.get('target', '-n'))
    if target.startswith('-'):
        cmd += [cmd_name, target]
    else:
        cmd += [cmd_name, '-t', target]
    return cmd


def bld_select_layout(cmd, cmd_name, cfg):
    """return list w/ values for select-layout"""
    layout = cfg.get('layout', False)
    if layout:
        try:
            layout = NAMED_LAYOUTS[layout]
        except KeyError:
            # it's not a named layout so use what they gave
            dbug('user named-layout (%s) not found.' % layout)
        cmd += [cmd_name, layout]
    else:
        cmd = []        # no op
    return cmd


def bld_select_pane(cmd, cmd_name, cfg):
    """return list w/ values for select-pane"""
    # tmux select-pane -t pane_id
    cmd += [cmd_name, '-t', str(cfg['pane_id'])]
    return cmd


def bld_attach_session(cmd, cmd_name, cfg):
    """return list w/ values for attach-session"""
    colors = get_colors(cfg)
    if colors:
        cmd.append(colors)
    cmd += [cmd_name, '-t', cfg['session-name']]
    return cmd


def build_command(cmd_name, cfg):
    """build a tmux command by dispatching to a bld_ function"""
    cmd = ['tmux']

    dispatch = {
        'new-session': bld_new_session,
        'new-window': bld_new_window,
        'rename-window': bld_rename_window,
        'send-keys': bld_send_keys,
        'split-window': bld_split_window,
        'select-window': bld_select_window,
        'select-layout': bld_select_layout,
        'select-pane': bld_select_pane,
        'attach-session': bld_attach_session,
    }
    try:
        cmd = dispatch[cmd_name](cmd, cmd_name, cfg)
    except KeyError:
        sys.exit('Abort: Unknown cmd (%s)' % cmd_name)

    dbug('cmd:', cmd)
    return cmd


def send_command(cmd_name, cfg):
    """build the tmux cmd string and then execute it."""
    cmd = build_command(cmd_name, cfg)
    if cmd:
        # we wait for the return before proceeding
        try:
            result = subprocess.check_call(cmd)
        except subprocess.CalledProcessError:
            print('Could not execute cmd: %s' % cmd)
            sys.exit('ABEND: command execution failure.')
        return result


def do_pane(cfg, create_pane):
    """create pane, config and return"""
    if create_pane:
        send_command('split-window', cfg)

    for keys_to_send in cfg.get('send-keys', []):
        send_command('send-keys', {'keys': keys_to_send})


def do_window(cfg, create_window):
    """set up a window and return"""
    if create_window:
        send_command('new-window', cfg)
    else:
        send_command('rename-window', cfg)

    create = False
    for pane in cfg['panes']:
        do_pane(pane, create)
        create = True
    # make the first pane created active
    send_command('select-pane', {'pane_id': 0})
    send_command('select-layout', cfg)


def do_session(cfg):
    """stand up the desired session and return"""
    if 'session-name' not in cfg:
        sys.exit('ABEND: session-name must be specified.')
    cwd = os.getcwd()
    try:
        send_command('new-session', cfg)
    except subprocess.CalledProcessError:
        sys.exit('ABEND: session-name (%s) already exists.' %
                 cfg['session-name'])
    os.chdir(cwd)


def process(cfg):
    """stand up a tmux session based on the config(cfg)"""
    do_session(cfg)
    create = False      # creating a session, already created
    windows = cfg.get('windows', [])
    for window in windows:
        do_window(window, create)
        create = True
    if len(windows) > 1:  # nothing to select if only 1 window
        send_command('select-window', cfg)


def populate_named_layouts(tmuxconf='~/.tmux.conf'):
    """read through ~/.tmux.conf and populate NAMED_LAYOUTS"""
    try:
        with open(os.path.expanduser(tmuxconf)) as h_tmux:
            for line in h_tmux:
                if line.startswith('# named-layout:'):
                    try:
                        _, name, layout = line.strip().split(':')
                        NAMED_LAYOUTS[name.strip()] = layout.strip()
                    except ValueError:
                        dbug('!could not read line: %s' % line.strip())
    except:     # pylint:disable=w0702
        # skip if ~/.tmux.conf is not found - python 2 and 3 have different
        # errors for this, but net result is we couldn't read the file
        dbug('!Could not read: ~/.tmux.conf')
    dbug('named-layouts:', NAMED_LAYOUTS)


def dbug(*args):
    """print out debug info, if in debug mode"""
    if DEBUG:
        print(*args)


def handle_meta_configs(args):
    """process arguments, look for meta config files, remove from the list of
    args and insert their elements in to args. Repeating until no meta config
    files are found, because a meta can point to another meta."""
    new_args = []
    meta_found = True
    while meta_found:
        meta_found = False
        for arg in args:
            dbug('hmc: %s' % arg)
            try:
                with open(arg) as h_arg:
                    contents = json.loads(h_arg.read())
                    if isinstance(contents, list):
                        dbug('hmc:meta found (%s)' % arg)
                        new_args += [os.path.expanduser(x) for x in contents]
                        meta_found = True
                    else:
                        new_args.append(arg)
            except:     # pylint:disable=w0702
                sys.exit('ABEND: (%s) Not Found, Not Readable or Not valid'
                          'JSON.' % arg)
        args = new_args[:]
        new_args = []
        dbug('current args:', args)

    return args


def main(cfg_names):
    """handle arguments and config names"""
    cfg_names = handle_debug_flag(cfg_names)
    cfg_names = handle_meta_configs(cfg_names)
    dbug('configs to process:', cfg_names)
    populate_named_layouts()
    first_attach = ''
    for cfg_path in cfg_names:
        dbug('processing:', cfg_path)
        try:
            with open(cfg_path) as h_cfg:
                jbuf = h_cfg.read()
        except:     # pylint:disable= w0702
            sys.exit('ABEND: Config file (%s) not found' % cfg_path)

        try:
            config = json.loads(jbuf)
        except ValueError:
            sys.exit('ABEND: (%s) is not valid json' % cfg_path)

        process(config)
        if not first_attach:
            if config.get('attach-session', False):
                dbug('attach ', config['session-name'])
                first_attach = config

    # only attempt to attach to the first session created w/ attach-session
    if first_attach:
        send_command('attach-session', first_attach)


if __name__ == "__main__":
    main(sys.argv[1:])

