tmx
===

a lightweight tool to quickly standup tmux sessions

tagline
-------

Taking the you out of tmux since 2015.

Goals
-----

 * minimal/no install required
 * Python 2/3 compatible
 * no external dependencies beyond tmux and python
 * simple configuration

Why not X, Y, Z or tmux cmds from a shell script?
-------------------------------------------------

Other projects, tmuxinator, teamocil and tmuxp, already exist and do an
excellent job. However, they all require an install. I wanted something
that did a similiar function but witout me having to install and maintain
another package.  Also, since it is no install setup, I can use my directory
synchronization tool to automatically make the script available everywhere 
I work and with a bash alias, it is accesible like any other tool.

I initially tried just using tmux commands in a shell script and it worked but
for some reason it didn't feel right.  It felt repetitive, wordy and a bit
brittle. With the number of similiar projects, I imagine this is a common
experience.

Install
-------

Download the latest version of the script to somewhere useful.  If you use a
directory synchronization tool (dropbox, syncthing, et al) that would be a
great place. *Remember* tmx.py is Python 2/3 compatible and doesn't require an
install so it works beautfully in this environment.

::

        cd ~/somewhere/useful
        wget https://bitbucket.org/dundeemt/tmx/raw/tip/tmx.py 


Running
-------

* Old School - you can run via traditional
  ``python ~/somewhere/useful/tmx.py``
* Modern - you can symlink it into your local bin directory. Less ugly but more work.

::

        ln -s ~/somewhere/userful/tmx.py ~/bin/tmx
        chmod +x ~/bin/tmx


* Preferred - use a Bash alias to do the dirty work, and since you probably already share your 
  .bash_aliases via your synchronized directories tool this means ``tmx`` will be available on 
  all your systems with no more work on your part.
  ``alias tmx='python ~/somewhere/useful/tmx.py'``

Session Configuration
---------------------

Configuration persistence is accomplished via JSON files.  While I generally prefer YAML, that
would require an external dependency, so JSON it is. The simplest configuration file possible
with tmx is:
    ``{"session-name": "my session name"}``
save your config file somewhere that makes sense to you.  Then to stand up that tmux session:
(assuming you named your file minimal.json)
    ``tmx ~\makes\sense\to\me\minimal.json``

.. sidebar:: Wow, It looks like nothing happened!

    tmx is a tool to standup tmux sessions, you can tell it to attach to the
    session but by default it does not.  I stand up multiple sessions
    normally and don't want to attach until they are all up.  You can attach to
    the session with the normal `tmux a` command.  *Note* if typing that much
    is as ugly to you as it is to me, setup a shell alias.
      ``alias tma='tmux -2 attach`` .


Session level attributes
------------------------
session-name: (str) REQUIRED
        name of the session, only 1 session with this name can be running.

attach-session: (bool) default=false
        should you automatically attach to this session once it is started.
        If processing multiple sessions, then the first configuration processed with
        attach-session: true is connected to automatically.

cwd: (str) default=current directory
        the path that should be used as the default for this session.  If not specified, then
        the current directory is set as the default for the session.

colors: (int|null) default=256
        If not specified, then tmux is put in 256 color mode. (-2 flag). Possible values are:
        88 - for 88 color mode, null - to not specify a color mode and use tmux defaults.  If
        a value is given and it is not 88 or null, it is interpreted as 256.

name: (str) optional
    Name of the first window created by the session. Is overridden by the name,
    if present, in the first window level definition. Useful if you only have 1
    window in the session, so you don't have to create a window definition just
    to set the name. see: window level - name

windows: (list) optional
        a list of 1 or more window definition dictionaries.

Window level attributes
-----------------------
name: (str) default=tmux naming scheme
        name of the window. If not specified named by tmux. see ``man tmux``

cwd: (str) default=session cwd
        path that should be used for panes in this window.

layout: (str) optional
        one of the 5 tmux defined layouts, a layout definition or a user named layout.
        see layout section.

panes: (list) optional
        a list of 1 or more pane definition dictionaries.

Pane level attributes
---------------------
cwd: (str) default=current window cwd
        path that should be set for this pane

split: (str) default=vertical
        how this pane should be split with reference to the previous pane. valid values are
        v or vertical (default), h or horizontal, or the pipe symbol(|) for left/right 
        split or the minus symbol (-) for a top/bottom split.

send-keys: (list)
        a list of 1 or more strings of keys to send the terminal in that pane.  Each 
        element of the list is sent to the terminal followed by <Enter> (C-m). see 
        ``man tmux`` for more details.

Philosophy of tmx
-----------------

With tmx, the first session config that has ``attach-session: true`` is the
session that tmx will tmux to use.  The first window you configured in the config file
will be the window you attach to within the session and the first pane you
configure will be the pane with focus. 

Session Configuration
---------------------

Ok, here is a minimal config file (min-attach.json) with attach:

::

        {
            "session-name": "minimal-attach",
            "attach-session": true
        }


by default, we get a session named "minimal-attach", with a single window and pane.  Also, 
we attach to that session.  You'll note that the current working directory(cwd) is wherever 
you ran the ``tmx minimal-attach.json`` command.  Each window created will default to 
wherever you issue the command.  Use the ``cwd`` attribute to change that.

::

        {
            "session-name": "minimal-attach",
            "attach-session": true,
            "cwd": "~/over/there"
        }


This will start the session at ~/over/there so the windows inherit that default path. You 
can use the ``cwd`` attribute on other windows and panes instead of using ``send-keys`` to
manipulate the path.  If you use indirect pathing on the other windows/panes it will use the 
session path as the starting point, otherwise use a full path. Yes, path expansions work, ``~``.

The last session level attribute is ``colors`` and you shouldn't have to worry about it, as like 
screen, it forces the session in to 256 color mode.  If you need 88 colors, use ``"colors": 88``, 
if you don't want it to change colors at all, then you will need to set colors to null. 
i.e. ``"colors": null``.  *Note* those are integer parameters or the json null value.


Windows
-------
*Important*: The first window created in the session, inherits it's cwd from the session.  The 
second window and on are controlled by the configuration settings.  The same is true for the first 
pane of each window. The first pane inherits the cwd of it's parent.

You can set the following attributes on a window: name, cwd, panes and layout. How about we have a 
look at 2 window config.

::


        {"session-name": "myproject",
         "cwd": "~/projects/myproject",
         "attach-session": true,
         "windows": [
                {"name": "vim",
                 "panes": [
                        {"send-keys": ["vim source.py"]}
                  ]
                },
                {"name": "server",
                 "cwd": "logs",
                 "panes":[
                        {"send-keys": ["tail -f mylog"]},
                        {"split": "|",
                         "cwd": "~/somewhere/else",
                         "send-keys": ["ls -alh"]}
                 ]
                }
            ]
        }


So the config starts off with session-name, cwd and setting attach-session to true, so we'll connect to
this session as soon as it is stood up.

Next is windows, which is a list of window configuration dictionaries.  Those consist of name, cwd, panes
and layout. (We'll cover layout in a bit.) The first window is named "vim", has a single pane and we are
using send-keys to fire up vim on some source code.

The next window is named, "server". We set the path for that to ~/projects/myproject/logs since the session
is already at ~/projects/myproject we can indirectly specify the path.  The window is split into two panes.
split left and right tail'ing a log file in the left and doing a directory listing in a completely different
path.  *Note* that you need to specify the complete path for that.

split
-----

The split attribute can take horizontal, vertical, -h, -v and since I always get tmux's screen splitting 
turned wrong way round. You can use a pipe symbol (|) to denote a left-right split, or a minus symbol (-) for a 
top-bottom split. Split's happen the same way tmux does them.  If you want to fine tune your splits, read on 
through the section on the layout attribute. Just remember, as each pane is created it becomes active, so the 
next pane created and split is in refence to the previously created pane.

size
-----

Affects the size of the new pane created by the split.  If not specified, then
tmux default sizing takes place.  The value of this attribute can be specified as a
number in which case it is specifying the exact number of rows or columns to
size the new pane.  Or it can be listed as a percentage by appending a
percent sign to the end of the value. i.e. "39%"


send-keys
---------

The send-keys attribute is a list of strings. It sends the characters to the pane/terminal. Each string has an
<Enter> (C-m) sent after it.  You can list multiple string elements or just one with semi-colons to seperate 
commands.  I like the ability to use a list, as I can format the json to read more easily if there a number of 
commands to be run to setup the pane.

layout attribute
----------------
Since we all like to fiddle with layout, I'm devoting a section to it here.  The layout attribute goes in the window section with name, cwd and panes.  You can use any of the named layouts as defined by tmux. (main-vertical, main-horizontal, even-vertical, even-horizontal or tiled) See ``man tmux`` for details.  You can also exactly define your layout by using the output of ``tmux lsw``

for instance:

::

        $ tmux lsw
        1: vim* (1 panes) [153x50] [layout af3d,153x50,0,0,0] @0 (active)
        2: server- (2 panes) [153x50] [layout ff9d,153x50,0,0{101x50,0,0,1,51x50,102,0,2}] @1



That string right after [layout for window 2, **ff9d,153x50,0,0{101x50,0,0,1,51x50,102,0,2}**
You could set your layout attribute like so:  ``"layout": "ff9d,153x50,0,0{101x50,0,0,1,51x50,102,0,2}"``

Now you can use the standard tmux named layouts, or use your own hand crafted layout.  Pretty handy, eh?  Well there is one more method. You can create your own named layouts.  You can save your named layouts in comments in the ~/.tmux.conf file.  I ended up using a standard group of hand crafted layouts and became bored and annoyed with cutting and pasting, so thus user defined named layouts were created.

Partial ~/.tmux.conf file:

::

        ...
        # named-layout: LEFTright: ff9d,153x50,0,0{101x50,0,0,1,51x50,102,0,2}
        # named-layout: TOPbottom: ....


The comment needs to start with ``# named-layout:`` (hash space named-layout colon) The next bit is the name of your user defined layout, then a colon and then the details from ``tmux lsw``  Don't worry about leading or trailing white space around the name or definition as it is removed when read. Now you can use ``"layout": "LEFTright"`` in your session configuration file.  If you keep your dotfiles under revision control, well then now you have your named layouts traveling wherever you go.

debug information
-----------------

Just put a ``--debug`` on your tmx command line and detail about what command is being issued 
and what bits make up the command are pumped out on stdout.  It's currently tmx's only option 
until we get a help flag put in place.  Most of the time you won't need it, but hey if you do, 
you do. *Note* to see the output if you are auto attaching, you will need to disconnect from 
your session to get back to the originating terminal to see the output.

Meta Configs
------------
I've become quite a bit lazier and dread the fact I'm typing:
        ``tmx ~/path/to/thisconfig.json ~/path/to/thatconfig.json ..```
You did realize that you can specify 1 or more config files and stand up all those tmux sessions in a jiffy, right?  Well, I have added meta config file type. JSON, of course, that just lists all the config files I want to open at once.  tmx will process everything in the cmd line, append on those configs found in meta config files and off we go to the races.  Now you can type ``tmx meta.json`` and open all my sessions with considerably less typing.  Here is how a meta config file is formatted:

::


        [
        "~/projects/myproject/myproject.json",
        "~/projects/otherproject/otherproject.json",
        "~/some/place/else/task.json"
        ]


It is just a list of config files or other meta files to process.  There is nothing special about the name or it's position in the argument list.  Each config file specified on the command line is evaluated and if it is a list, then it is a meta file, the files listed within have their path's expanded and are inserted into the list of files to process.  If you are evil, or bored, meta files can be nested.  ``tmx`` will just keep processing them until no more meta files are encountered.  It will then start standing up tmux sessions based on the new expanded list.


Bugs and Patches
----------------
Are welcomed and encouraged. The only basic rules are: python 2/3 compatible, conform to pep8, tests are kings and no 3rd party packages.  tmx should stay a single script file for ease of use. 


Future
------

I do not see tmx becoming much more involved than this.  If it were, then it would just be another in a crowded group of already decent utilities. And there is not much point in that.  Hope you like it.


